#include <stdio.h>
#include <stdlib.h>
#include "fforth.h"

// Implement history using libreadline
#include <readline/readline.h>
#include <readline/history.h>

FICL_PLATFORM_EXTERN int input_init(const char *name) {
    return 0;
}

FICL_PLATFORM_EXTERN int input_get(char *buffer, size_t buffersize) {
    char *input = readline(FICL_PROMPT);
    if (!input) return EOF;
    strlcpy(buffer, input, buffersize);
    add_history(buffer);

    free(input);

    return 0;
}

FICL_PLATFORM_EXTERN int input_finish() {
    return 0;
}
