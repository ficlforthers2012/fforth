/*
** Ficl softcore generator.
** Generates both uncompressed and Lempel-Ziv compressed versions.
** Strips blank lines, strips full-line comments, collapses whitespace.
** Chops, blends, dices, makes julienne fries.
**
** Contributed by Larry Hastings, larry@hastings.org
**
** Adapted to use standard zlib bindings by Tony Sidaway <tonysidaway@gmail.com>
** Now uses DEFLATE algorithm.
**/
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <zlib.h>
#include <assert.h>

#include "ficl.h"


void fprintDataAsHex(FILE *f, unsigned char *data, int length)
	{
	int i;
	while (length)
		{
		fprintf(f, "\t");
		for (i = 0; (i < 8) && length; i++)
			{
			char buf[16];
			/* if you don't do this little stuff, you get ugly sign-extended 0xFFFFFF6b crap. */
			snprintf(buf, sizeof(buf), "%08x", (unsigned int)*data++);
			fprintf(f, "0x%s, ", buf + 6);
			length--;
			}
		fprintf(f, "\n");
		}
	}

void fprintDataAsQuotedString(FILE *f, unsigned char *data)
	{
	int i;
	int lineIsBlank = 1; /* true */

	while (*data)
		{
		if (*data == '\n')
			{
			if (!lineIsBlank)
				fprintf(f, "\\n\"\n");
			lineIsBlank = 1; /* true */
			}
		else
			{
			if (lineIsBlank)
				{
				fputc('\t', f);
				fputc('"', f);
				lineIsBlank = 0; /* false */
				}

			if (*data == '"')
				fprintf(f, "\\\"");
			else if (*data == '\\')
				fprintf(f, "\\\\");
			else
				fputc(*data, f);
			}
		data++;
		}
	if (!lineIsBlank)
		fprintf(f, "\"");
	}

int main(int argc, char *argv[])
	{
	char *softcore_out = argv[1];
	unsigned char *uncompressed = (unsigned char *)malloc(128 * 1024);
	unsigned char *compressed;
	unsigned char *trace = uncompressed;
	int i;
	size_t compressedSize;
	size_t uncompressedSize;
	unsigned char *src, *dst;
	FILE *f;
	time_t currentTimeT;
	struct tm *currentTime;
	char cleverTime[32];

	// printf("Output file is %s\n", softcore_out);

	time(&currentTimeT);
	currentTime = localtime(&currentTimeT);
	strftime(cleverTime, sizeof(cleverTime), "%Y/%m/%d %H:%M:%S", currentTime);

	*trace++ = ' ';

	for (i = 2; i < argc; i++)
		{
		int size;
		// printf ("Processing input file %s\n", argv[i]);
		/*
		** This ensures there's always whitespace space between files.   It *also*
		** ensures that src[-1] is always safe in comment detection code below.
		** (Any leading whitespace will be thrown away in a later pass.)
		** --lch
		*/
		*trace++ = ' ';

		f = fopen(argv[i], "rb");
		if (NULL == f) exit(-1);
		fseek(f, 0, SEEK_END);
		size = ftell(f);
		fseek(f, 0, SEEK_SET);
		fread(trace, 1, size, f);
		fclose(f);
		trace += size;
		}
	*trace = 0;
	
#define IS_EOL(x) ((*x == '\n') || (*x == '\r'))
#define IS_EOL_COMMENT(x) (((x[0] == '\\') && isspace(x[1]))  || ((x[0] == '/') && (x[1] == '/') && isspace(x[2])))
#define IS_BLOCK_COMMENT(x) ((x[0] == '(') && isspace(x[1]) && isspace(x[-1]))

	src = dst = uncompressed;
	while (*src)
		{
		/* ignore leading whitespace, or entirely blank lines */
		while (isspace(*src))
			src++;
		/* if the line is commented out */
		if (IS_EOL_COMMENT(src))
			{
			/* throw away this entire line */
			while (*src && !IS_EOL(src))
				src++;
			continue;
			}
		/*
		** This is where we'd throw away mid-line comments, but
		** that's simply unsafe.  Things like
		**      start-prefixes
		**      : \ postpone \ ;
		**      : ( postpone ( ;
		** get broken that way.
		** --lch
		*/
		while (*src && !IS_EOL(src))
		{
			*dst++ = *src++;
		}

		/* strip trailing whitespace */
		dst--;
		while (isspace(*dst))
			dst--;
		dst++;

		/* and end the line */
		*dst++ = '\n';
		}

	*dst = 0;

	/* now make a second pass to collapse all contiguous whitespace to a single space. */
	src = dst = uncompressed;
	while (*src)
	{
		*dst++ = *src;
		if (!isspace(*src))
			src++;
		else
		{
			while (isspace(*src))
				src++;
		}
	}
	*dst = 0;

	f = fopen(softcore_out, "wt");
	if (f == NULL)
		{
		printf("couldn't open output file %s for writing!  giving up.\n", softcore_out);
		exit(-1);
		}

	fprintf(f,
"/*\n"
"** Ficl softcore\n"
#if FICL_PLATFORM_HAS_ZLIB
"** both uncompressed and Lempel-Ziv compressed versions.\n"
#endif
"**\n"
"** Generated %s\n"
"**/\n"
"\n"
"#include \"ficl.h\"\n"
"\n"
"\n",
	cleverTime);
	
	uncompressedSize = dst - uncompressed;
	fprintf(f, "static size_t ficlSoftcoreUncompressedSize = %d; /* not including trailing null */\n", uncompressedSize);
#if FICL_PLATFORM_HAS_ZLIB
	// ficlLzCompress(uncompressed, uncompressedSize, &compressed, &compressedSize);
	compressed = malloc(uncompressedSize);
	assert(NULL != compressed);
	compressedSize = ficlZlibDeflate(Z_DEFAULT_COMPRESSION, uncompressed, uncompressedSize, compressed, uncompressedSize);
	assert(compressedSize > 0);

	fprintf(f, "static size_t ficlSoftcoreCompressedSize = %d; /* not including trailing null */\n", compressedSize);
	fprintf(f, "\n");
	fprintf(f, "#if !FICL_WANT_LZ_SOFTCORE\n");
	fprintf(f, "\n");
#endif
	fprintf(f, "static unsigned char ficlSoftcoreUncompressed[] =\n");
	fprintDataAsQuotedString(f, uncompressed);
	fprintf(f, ";\n");
	fprintf(f, "\n");
#if FICL_PLATFORM_HAS_ZLIB
	fprintf(f, "#else /* !FICL_WANT_LZ_SOFTCORE */\n");
	fprintf(f, "\n");
	fprintf(f, "static unsigned char ficlSoftcoreCompressed[%d] = {\n", compressedSize);
	fprintDataAsHex(f, compressed, compressedSize);
	fprintf(f, "\t};\n");
	fprintf(f, "\n");
	fprintf(f, "#endif /* !FICL_WANT_LZ_SOFTCORE */\n");
#endif
	fprintf(f,
"\n"
"\n"
"void ficlSystemCompileSoftCore(ficlSystem *system)\n"
"{\n"
"    ficlVm *vm = system->vmList;\n"
"    int returnValue;\n"
"    ficlCell oldSourceID = vm->sourceId;\n"
"    ficlString s;\n"
#if FICL_PLATFORM_HAS_ZLIB
"#if FICL_WANT_LZ_SOFTCORE\n"
"    unsigned char *ficlSoftcoreUncompressed = (unsigned char *)malloc(ficlSoftcoreUncompressedSize);\n"
"    size_t gotUncompressedSize = 0;\n"
"    // returnValue = ficlLzUncompress(ficlSoftcoreCompressed, (unsigned char **)&ficlSoftcoreUncompressed, &gotUncompressedSize);\n"
"    gotUncompressedSize = ficlZlibInflate(ficlSoftcoreCompressed, ficlSoftcoreCompressedSize, ficlSoftcoreUncompressed, ficlSoftcoreUncompressedSize);\n"
"    // FICL_VM_ASSERT(vm, returnValue == 0);\n"
"    FICL_VM_ASSERT(vm, gotUncompressedSize == ficlSoftcoreUncompressedSize);\n"
"#endif /* FICL_WANT_LZ_SOFTCORE */\n"
#endif
"    vm->sourceId.i = -1;\n"
"    FICL_STRING_SET_POINTER(s, (char *)(ficlSoftcoreUncompressed));\n"
"    FICL_STRING_SET_LENGTH(s, ficlSoftcoreUncompressedSize);\n" 
"    returnValue = ficlVmExecuteString(vm, s);\n"
"    vm->sourceId = oldSourceID;\n"
#if FICL_PLATFORM_HAS_ZLIB
"#if FICL_WANT_LZ_SOFTCORE\n"
"    free(ficlSoftcoreUncompressed);\n"
"#endif /* FICL_WANT_LZ_SOFTCORE */\n"
#endif
"    FICL_VM_ASSERT(vm, returnValue != FICL_VM_STATUS_ERROR_EXIT);\n"
"    return;\n"
"}\n"
"\n"
"/* end-of-file */\n"
		);
	free(uncompressed);
#if FICL_PLATFORM_HAS_ZLIB
	free(compressed);
#endif
	return 0;
	}
