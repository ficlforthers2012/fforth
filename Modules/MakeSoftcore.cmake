# Incorporate a module containing the Forth language code.
set (EXTRA_MAKESOFTCORE_SOURCE_FILES )

if (FICL_PLATFORM_HAS_ZLIB)
  set (EXTRA_MAKESOFTCORE_SOURCE_FILES ${EXTRA_MAKESOFTCORE_SOURCE_FILES} zlibwrapper.c)
endif (FICL_PLATFORM_HAS_ZLIB)

add_executable(makesoftcore softcore/makesoftcore.c ficl.h ${EXTRA_MAKESOFTCORE_SOURCE_FILES})

if (FICL_PLATFORM_HAS_ZLIB)
  target_link_libraries (makesoftcore z)
endif (FICL_PLATFORM_HAS_ZLIB)

set(SOFTCORE_FORTH_SOURCE_FILES ${PROJECT_SOURCE_DIR}/softcore/softcore.fr ${PROJECT_SOURCE_DIR}/softcore/ifbrack.fr ${PROJECT_SOURCE_DIR}/softcore/prefix.fr ${PROJECT_SOURCE_DIR}/softcore/ficl.fr ${PROJECT_SOURCE_DIR}/softcore/jhlocal.fr ${PROJECT_SOURCE_DIR}/softcore/marker.fr ${PROJECT_SOURCE_DIR}/softcore/oo.fr ${PROJECT_SOURCE_DIR}/softcore/classes.fr ${PROJECT_SOURCE_DIR}/softcore/string.fr ${PROJECT_SOURCE_DIR}/softcore/ficllocal.fr ${PROJECT_SOURCE_DIR}/softcore/fileaccess.fr)


add_custom_command(  OUTPUT softcore.c
                     DEPENDS makesoftcore ${SOFTCORE_FORTH_SOURCE_FILES}
		     COMMAND makesoftcore ${PROJECT_BINARY_DIR}/softcore.c ${SOFTCORE_FORTH_SOURCE_FILES}
		  )

