# Some logic to provide two string functions missing from, say, GNU libc.
check_function_exists(strlcpy FICL_PLATFORM_HAS_STRCPY)
check_function_exists(strlcat FICL_PLATFORM_HAS_STRCAT)

if (NOT FICL_PLATFORM_HAS_STRCPY)
    set(EXTRA_FICL_LIB_SOURCES ${EXTRA_FICL_LIB_SOURCES} strlcpy.c)
endif (NOT FICL_PLATFORM_HAS_STRCPY)

if (NOT FICL_PLATFORM_HAS_STRCAT)
    set(EXTRA_FICL_LIB_SOURCES ${EXTRA_FICL_LIB_SOURCES} strlcat.c)
endif (NOT FICL_PLATFORM_HAS_STRCAT)
