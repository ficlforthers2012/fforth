#Some logic to decide what to do about command line history.

set(HISTORY_SELECTED 0)
if (WANT_HISTORY)
  set(CMAKE_REQUIRED_LIBRARIES termcap)
  check_library_exists (edit el_init /usr/lib/ PLATFORM_HAS_LIBEDIT)
  set(CMAKE_REQUIRED_LIBRARIES )
  check_library_exists (readline readline /usr/lib/ PLATFORM_HAS_READLINE)

  if ((PLATFORM_HAS_LIBEDIT) AND
     ((NOT PLATFORM_HAS_READLINE) OR (NOT PREFER_READLINE)))
    set(HISTORY_SELECTED 1)
    set(EXTRA_FFORTH_SOURCES ${EXTRA_FFORTH_SOURCES} input_libedit.c)
    set(EXTRA_FFORTH_LIBS ${EXTRA_FFORTH_LIBS} edit termcap)
  endif ((PLATFORM_HAS_LIBEDIT) AND
     ((NOT PLATFORM_HAS_READLINE) OR (NOT PREFER_READLINE)))

  if ((PLATFORM_HAS_READLINE) AND
     ((NOT PLATFORM_HAS_LIBEDIT) OR (PREFER_READLINE)))
    set(HISTORY_SELECTED 1)
    set(EXTRA_FFORTH_SOURCES ${EXTRA_FFORTH_SOURCES} input_readline.c)
    set(EXTRA_FFORTH_LIBS ${EXTRA_FFORTH_LIBS} readline)
  endif ((PLATFORM_HAS_READLINE) AND
     ((NOT PLATFORM_HAS_LIBEDIT) OR (PREFER_READLINE)))
endif (WANT_HISTORY)

if ((NOT HISTORY_SELECTED))
  # Use a fallback input module that retains no history.
  set(EXTRA_FFORTH_SOURCES ${EXTRA_FFORTH_SOURCES} input_noedit.c)
endif ((NOT HISTORY_SELECTED))
