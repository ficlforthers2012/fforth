/*******************************************************************
** z l i b w r a p p e r . c
** Forth Inspired Command Language
** Author: John Sadler (john_sadler@alum.mit.edu)
** Created: 16 Oct 1997
*******************************************************************/
/*
** Copyright (c) 1997-2001 John Sadler (john_sadler@alum.mit.edu)
** All rights reserved.
**
** Get the latest Ficl release at http://ficl.sourceforge.net
**
** I am interested in hearing from anyone who uses Ficl. If you have
** a problem, a success story, a defect, an enhancement request, or
** if you would like to contribute to the Ficl release, please
** contact me by email at the address above.
**
** L I C E N S E  and  D I S C L A I M E R
** 
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
** 1. Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
** 2. Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in the
**    documentation and/or other materials provided with the distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
** OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
** OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
** SUCH DAMAGE.
*/
/* This module added by Tony Sidaway <tonysidaway@gmail.com>, to replace
** the hand-rolled Lempel-Ziv code, which has bugs that manifest on 64-bit
** architectures. Using the well tested zlib is preferable.
*/
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <zlib.h>
#include "ficl.h"

FICL_PLATFORM_EXTERN int ficlZlibDeflate(const int level, const unsigned char *src, const int srclen, unsigned char *dest, const int destlen) {
    z_stream zs;
    int ret, err;
    zs.zalloc = Z_NULL;
    zs.zfree = Z_NULL;
    zs.opaque = Z_NULL;
    zs.total_in = zs.avail_in = srclen;
    zs.total_out = zs.avail_out = destlen;
    zs.next_in = (unsigned char *)src;
    zs.next_out = dest;
    ret = -1;
    // printf("Compressing %d characters\n", srclen);
    err = deflateInit(&zs, level);
    if (Z_OK == err) {
	err= deflate(&zs, Z_FINISH);
	if (Z_STREAM_END == err)
	    ret = zs.total_out;
	else
	    printf("Return code from deflate is %d\n", err);
    }
    deflateEnd(&zs);
    return ret;
}
 

FICL_PLATFORM_EXTERN int ficlZlibInflate(const unsigned char *src, const int srclen, unsigned char *dest, const int destlen) {
    z_stream zs;
    int ret, err;
    zs.zalloc = Z_NULL;
    zs.zfree = Z_NULL;
    zs.opaque = Z_NULL;
    zs.total_in = zs.avail_in = srclen;
    zs.total_out = zs.avail_out = destlen;
    zs.next_in = (unsigned char *)src;
    zs.next_out = dest;
    ret = -1;
    // printf("Uncompressing %d characters\n", destlen);
    err = inflateInit(&zs);
    if (Z_OK == err) {
	err = inflate(&zs, Z_FINISH);
	if (Z_STREAM_END == err)
	    ret = zs.total_out;
	else
	    printf("Return code from inflate is %d\n", err);
    }
    inflateEnd(&zs);
    return ret;
}
