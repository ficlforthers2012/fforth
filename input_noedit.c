#include <stdio.h>
#include <stdlib.h>
#include "fforth.h"

// Don't implement history


FICL_PLATFORM_EXTERN int input_init(const char *name) {
    return 0;
}

FICL_PLATFORM_EXTERN int input_get(char *buffer, size_t buffersize) {
    fputs(FICL_PROMPT, stdout);
    if (fgets(buffer, buffersize, stdin) == NULL) return EOF;
    return 0;
}

FICL_PLATFORM_EXTERN int input_finish() {
    return 0;
}
