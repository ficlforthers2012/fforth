#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <histedit.h>
#include "fforth.h"

// Implement history using libedit
static EditLine *el;
static History *myhistory;
static HistEvent ev;

static char * prompt(EditLine *e) {
  return FICL_PROMPT;
}

FICL_PLATFORM_EXTERN int input_init(const char *name) {
    el = el_init(name, stdin, stdout, stderr);
    el_set(el, EL_PROMPT, &prompt);
    el_set(el, EL_EDITOR, "emacs");

    myhistory = history_init();
    if (myhistory == 0) {
	fprintf(stderr, "history could not be initialized\n");
	return 1;
    }

    history(myhistory, &ev, H_SETSIZE, 800);

    el_set(el, EL_HIST, history, myhistory);

    return 0;
}

FICL_PLATFORM_EXTERN int input_get(char *buffer, size_t buffersize) {
int count;
const char *line;
    line = el_gets(el, &count);
    if (count > 0) {
	strlcpy(buffer, line, buffersize);
	history(myhistory, &ev, H_ENTER, buffer);
    } else if (count < 0) {
	perror("err: ");
    }
    return (count > 0)? 0: -1;
}

FICL_PLATFORM_EXTERN int input_finish() {
    history_end(myhistory);
    el_end(el);
    return 0;
}
