/*******************************************************************
** f l o a t . c
** Forth Inspired Command Language
** ANS Forth FLOAT word-set written in C
** Author: Guy Carver & John Sadler (john_sadler@alum.mit.edu)
** Created: Apr 2001
** $Id: float.c,v 1.10 2010/09/13 18:43:04 asau Exp $
*******************************************************************/
/*
** Copyright (c) 1997-2001 John Sadler (john_sadler@alum.mit.edu)
** All rights reserved.
**
** Get the latest Ficl release at http://ficl.sourceforge.net
**
** I am interested in hearing from anyone who uses Ficl. If you have
** a problem, a success story, a defect, an enhancement request, or
** if you would like to contribute to the Ficl release, please
** contact me by email at the address above.
**
** L I C E N S E  and  D I S C L A I M E R
** 
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
** 1. Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
** 2. Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in the
**    documentation and/or other materials provided with the distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
** OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
** OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
** SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <inttypes.h>
#include "ficl.h"

#if FICL_FLOAT_IS_IEEE_DOUBLE
#define FLOOR floor
#define FMIN fmin
#define FMAX fmax
#define ROUND round
#define FABS fabs
#define ACOS acos
#define ACOSH acosh
#define POW pow
#define ASIN asin
#define ASINH asinh
#define ATAN atan
#define ATAN2 atan2
#define ATANH atanh
#define ACOS acos
#define COS cos
#define COSH cosh
#define EXP exp
#define EXPM1 expm1
#define LOG log
#define LOG1P log1p
#define LOG10 log10
#define SIN sin
#define SINH sinh
#define SQRT sqrt
#define TAN tan
#define TANH tanh
#define TRUNC trunc
#else
#define FLOOR floorf
#define FMIN fminf
#define FMAX fmaxf
#define ROUND roundf
#define FABS fabsf
#define ACOS acosf
#define ACOSH acoshf
#define POW powf
#define ASIN asinf
#define ASINH asinhf
#define ATAN atanf
#define ATAN2 atan2f
#define ATANH atanhf
#define ACOS acosf
#define COS cosf
#define COSH coshf
#define EXP expf
#define EXPM1 expm1f
#define LOG logf
#define LOG1P log1pf
#define LOG10 log10f
#define SIN sinf
#define SINH sinhf
#define SQRT sqrtf
#define TAN tanf
#define TANH tanhf
#define TRUNC truncf
#endif


/*******************************************************************
** Create a floating point constant.
** fconstant ( r -"name"- )
*******************************************************************/
static void ficlPrimitiveFConstant(ficlVm *vm)
{
    ficlDictionary *dictionary = ficlVmGetDictionary(vm);
    ficlString name = ficlVmGetWord(vm);

    FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 0);

    ficlDictionaryAppendWord(dictionary, name, (ficlPrimitive)ficlInstructionFConstantParen, FICL_WORD_DEFAULT);
    ficlDictionaryAppendCell(dictionary, ficlStackPop(vm->floatStack));
}


ficlWord   *ficlDictionaryAppendFConstant(ficlDictionary *dictionary, char *name, float value)
{
	ficlString s;
	FICL_STRING_SET_FROM_CSTRING(s, name);
	return ficlDictionaryAppendConstantInstruction(dictionary, s, ficlInstructionFConstantParen, *(ficlInteger *)(&value));
}


ficlWord   *ficlDictionarySetFConstant(ficlDictionary *dictionary, char *name, float value)
{
    ficlString s;
    FICL_STRING_SET_FROM_CSTRING(s, name);
    return ficlDictionarySetConstantInstruction(dictionary, s, ficlInstructionFConstantParen, *(ficlInteger *)(&value));
}

static void ficlPrimitiveFAlign(ficlVm *vm)
{
    // This is a stopgap. It is likely to work on most
    // hardware.
    ficlPrimitiveAlign(vm);
}

static void ficlPrimitiveFAligned(ficlVm *vm)
{
    // This is a stopgap. It is likely to work on most
    // hardware.
    ficlPrimitiveAligned(vm);
}

static void ficlPrimitiveFVariable(ficlVm *vm)
{
    // Stopgap. Should work on most architectures.
    ficlDictionary *dictionary = ficlVmGetDictionary(vm);
    ficlString name = ficlVmGetWord(vm);

    ficlDictionaryAppendWord(dictionary, name, (ficlPrimitive)ficlInstructionVariableParen, FICL_WORD_DEFAULT);
    ficlVmDictionaryAllotCells(vm, dictionary, 1);
    return;
}

static void ficlPrimitiveFTilde(ficlVm *vm)
{
    FICL_FLOAT_STACK_CHECK(vm->floatStack, 3, 0);
    FICL_STACK_CHECK(vm->dataStack, 0, 1);
    ficlInteger flag;
    ficlFloat r3 = ficlStackPopFloat(vm->floatStack);
    ficlFloat r2 = ficlStackPopFloat(vm->floatStack);
    ficlFloat r1 = ficlStackPopFloat(vm->floatStack);
    if (r3 > 0) flag = FABS (r1 - r2) < r3;
    else if (r3 < 0) flag = FABS (r1 - r2) < fabs (r3) * (fabs (r1) + fabs (r2));
    else flag = (r1 == r2);
    ficlStackPushInteger(vm->dataStack,
	flag? FICL_TRUE: FICL_FALSE);
    return;
}

static void ficlPrimitiveFloor (ficlVm *vm) //FLOOR
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, FLOOR (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFMax (ficlVm *vm) //FMAX
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 2, 1);
  ficlStackPushFloat(vm->floatStack, FMAX (ficlStackPopFloat(vm->floatStack),
					ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFMin (ficlVm *vm) //FMIN
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 2, 1);
  ficlStackPushFloat(vm->floatStack, FMIN (ficlStackPopFloat(vm->floatStack),
					ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFRound (ficlVm *vm) //ROUND
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, ROUND (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFAbs (ficlVm *vm) //FABS
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, FABS (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFACos (ficlVm *vm) //ACOS
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, ACOS (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFACosH (ficlVm *vm) //ACOSH
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, ACOSH (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFALog (ficlVm *vm) //POW(10.0, x)
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, POW(10.0, ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFASin (ficlVm *vm) //ASIN
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, ASIN (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFASinH (ficlVm *vm) //ASINH
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, ASINH (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFATan (ficlVm *vm) //ATAN
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, ATAN (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFATan2 (ficlVm *vm) //ATAN2
{
  float r1, r2;
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 2, 1);
  r2 = ficlStackPopFloat(vm->floatStack);
  r1 = ficlStackPopFloat(vm->floatStack);
  ficlStackPushFloat(vm->floatStack, ATAN2 (r1, r2));
  return;
}
static void ficlPrimitiveFATanH (ficlVm *vm) //ATANH
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, ATANH (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFCos (ficlVm *vm) //COS
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, COS (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFCosH (ficlVm *vm) //COSH
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, COSH (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFExp (ficlVm *vm) //EXP
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, EXP (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFExpM1 (ficlVm *vm) //EXPM1
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, EXPM1 (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFLn (ficlVm *vm) //LOG
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, LOG (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFLnP1 (ficlVm *vm) //LOG1P
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, LOG1P (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFLog (ficlVm *vm) //LOG10
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, LOG10 (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFSin (ficlVm *vm) //SIN
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, SIN (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFSinH (ficlVm *vm) //SINH
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, SINH (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFSqrt (ficlVm *vm) //SQRT
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, SQRT (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFTan (ficlVm *vm) //TAN
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, TAN (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFTanH (ficlVm *vm) //TANH
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, TANH (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFTrunc (ficlVm *vm) //TRUNC
{
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 1);
  ficlStackPushFloat(vm->floatStack, TRUNC (ficlStackPopFloat(vm->floatStack)));
  return;
}
static void ficlPrimitiveFStarStar (ficlVm *vm) //POW
{
  float r1, r2;
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 2, 1);
  r2 = ficlStackPopFloat(vm->floatStack);
  r1 = ficlStackPopFloat(vm->floatStack);
  ficlStackPushFloat(vm->floatStack, POW (r1, r2));
  return;
}
static void ficlPrimitiveFSinCos (ficlVm *vm) //SIN, COS
{
  float r1;
  FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 2);
  r1 = ficlStackPopFloat(vm->floatStack);
  ficlStackPushFloat(vm->floatStack, SIN (r1));
  ficlStackPushFloat(vm->floatStack, COS (r1));
  return;
}


/*******************************************************************
** Display a float in decimal format.
** f. ( r -- )
*******************************************************************/
static void ficlPrimitiveFDot(ficlVm *vm)
{
    ficlFloat f;

    FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 0);

    f = ficlStackPopFloat(vm->floatStack);
    snprintf(vm->vpad, sizeof(vm->vpad), "%#f ", f);
    ficlVmTextOut(vm, vm->vpad);
}

/*******************************************************************
** Display a float in engineering format.
** fe. ( r -- )
*******************************************************************/
static void ficlPrimitiveEDot(ficlVm *vm)
{
    ficlFloat f;

    FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 0);

    f = ficlStackPopFloat(vm->floatStack);
    snprintf(vm->vpad, sizeof(vm->vpad), "%#e ",f);
    ficlVmTextOut(vm, vm->vpad);
}

/**************************************************************************
                        d i s p l a y FS t a c k
** Display the parameter stack (code for "f.s")
** f.s ( -- )
**************************************************************************/
struct stackContext
{
    ficlVm *vm;
    int count;
};

static ficlInteger ficlFloatStackDisplayCallback(void *c, ficlCell *cell)
{
    struct stackContext *context = (struct stackContext *)c;
    char buffer[64];
    snprintf(buffer, sizeof(buffer), "[0x%08" PRIXPTR " %3d] %16f (0x%08lx)\n", (uintptr_t)cell, context->count++, *((ficlFloat *)cell), cell->i);
    ficlVmTextOut(context->vm, buffer);
	return FICL_TRUE;
}



void ficlVmDisplayFloatStack(ficlVm *vm)
{
    struct stackContext context;
	context.vm = vm;
	context.count = 0;
    ficlStackDisplay(vm->floatStack, ficlFloatStackDisplayCallback, &context);
    return;
}



/*******************************************************************
** Do float stack depth.
** fdepth ( -- n )
*******************************************************************/
static void ficlPrimitiveFDepth(ficlVm *vm)
{
    int i;

    FICL_STACK_CHECK(vm->dataStack, 0, 1);

    i = ficlFloatStackDepth(vm->floatStack);
    ficlStackPushInteger(vm->dataStack, i);
}

/*******************************************************************
** Compile a floating point literal.
*******************************************************************/
static void ficlPrimitiveFLiteralImmediate(ficlVm *vm)
{
    ficlFloat f;
    ficlDictionary *dictionary = ficlVmGetDictionary(vm);
	ficlCell cell;


    FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 0);


	f = ficlStackPopFloat(vm->floatStack);
	if (f == 1.0f)
	{
		ficlDictionaryAppendUnsigned(dictionary, ficlInstructionF1);
	}
	else if (f == 0.0f)
	{
		ficlDictionaryAppendUnsigned(dictionary, ficlInstructionF0);
	}
	else if (f == -1.0f)
	{
		ficlDictionaryAppendUnsigned(dictionary, ficlInstructionFNeg1);
	}
	else
	{
		ficlDictionaryAppendUnsigned(dictionary, ficlInstructionFLiteralParen);
		ficlDictionaryAppendCell(dictionary, cell);
	}
}

/**************************************************************************
                     F l o a t P a r s e S t a t e
** Enum to determine the current segement of a floating point number
** being parsed.
**************************************************************************/
#define NUMISNEG 1
#define EXPISNEG 2

typedef enum _floatParseState
{
    FPS_HEAD,
    FPS_START,
    FPS_ININT,
    FPS_INMANT,
    FPS_STARTEXP,
    FPS_INEXP,
    FPS_TAIL
} FloatParseState;

/**************************************************************************
                     p a r s e F l o a t N u m b e r
** vm -- Virtual Machine pointer.
** s -- String to parse.
** forthcode -- whether we're parsing Forth code (rules are tighter)

** Returns 1 if successful, 0 if not.
**************************************************************************/
static int parseFloatNumber( ficlVm *vm, ficlString s, int forthcode)
{
    unsigned char c;
	unsigned char digit;
    char *trace;
    ficlUnsigned length;
    ficlFloat power;
    ficlFloat accum = 0.0f;
    ficlFloat mant = 0.1f;
    ficlInteger exponent = 0;
    char flag = 0;
    FloatParseState estate = forthcode? FPS_START: FPS_HEAD;


    FICL_FLOAT_STACK_CHECK(vm->floatStack, 0, 1);

	
    /*
    ** floating point numbers only allowed in base 10 
    */
    if (vm->base != 10)
        return(0);


    trace = FICL_STRING_GET_POINTER(s);
    length = FICL_STRING_GET_LENGTH(s);

    /* Loop through the string's characters. */
    while ((length--) && ((c = *trace++) != 0))
    {
        switch (estate)
        {
	    case FPS_HEAD:
		if (isspace(c)) break;
		estate = FPS_START;
		/* Note we're dropping through to FPS_START */

            /* At start of the number so look for a sign. */
            case FPS_START:
            {
                estate = FPS_ININT;
                if (c == '-')
                {
                    flag |= NUMISNEG;
                    break;
                }
                if (c == '+')
                {
                    break;
                }
            } /* Note!  Drop through to FPS_ININT */
            /*
            **Converting integer part of number.
            ** Only allow digits, decimal and 'E'. 
            */
            case FPS_ININT:
            {
                if (c == '.')
                {
                    estate = FPS_INMANT;
                }
		else if (!forthcode && isspace(c))
		{
		    estate = FPS_TAIL;
		}
                else if ((c == 'e') || (c == 'E')
                || (!forthcode && (((c == 'd') || (c == 'D')))))
                {
                    estate = FPS_STARTEXP;
                }
                else if (isdigit(c))
                {
                    digit = (unsigned char)(c - '0');
                    if (digit > 9)
                        return(0);

                    accum = accum * 10 + digit;

                }
		else
		{
		    return 0;
		}
                break;
            }
            /*
            ** Processing the fraction part of number.
            ** Only allow digits and 'E' 
            */
            case FPS_INMANT:
            {
		if (!forthcode && isspace(c))
		{
		    estate = FPS_TAIL;
		    break;
		}
                if ((c == 'e') || (c == 'E')
                || (!forthcode && (((c == 'd') || (c == 'D')))))
                {
                    estate = FPS_STARTEXP;
                }
                else
                {
                    digit = (unsigned char)(c - '0');
                    if (digit > 9)
                        return(0);

                    accum += digit * mant;
                    mant *= 0.1f;
                }
                break;
            }
            /* Start processing the exponent part of number. */
            /* Look for sign. */
            case FPS_STARTEXP:
            {
                estate = FPS_INEXP;

                if (c == '-')
                {
                    flag |= EXPISNEG;
                    break;
                }
                else if (c == '+')
                {
                    break;
                }
            }       /* Note!  Drop through to FPS_INEXP */
            /*
            ** Processing the exponent part of number.
            ** Only allow digits. 
            */
            case FPS_INEXP:
            {
		if (!forthcode && isspace(c))
		{
		    estate = FPS_TAIL;
		    break;
		}
		if (!isdigit(c)) return 0;
                digit = (unsigned char)(c - '0');
                if (digit > 9)
                    return(0);

                exponent = exponent * 10 + digit;

                break;
            }
	    case FPS_TAIL:
		if (!isspace(c)) return 0;
		break;
        }
    }

    /* If parser never made it to the exponent this is not a float. */
    if (forthcode && (estate < FPS_STARTEXP))
        return(0);

    /* Set the sign of the number. */
    if (flag & NUMISNEG)
        accum = -accum;

    /* If exponent is not 0 then adjust number by it. */
    if (exponent != 0)
    {
        /* Determine if exponent is negative. */
        if (flag & EXPISNEG)
        {
            exponent = -exponent;
        }
        /* power = 10^x */
        power = POW(10.0, exponent);
        accum *= power;
    }

    ficlStackPushFloat(vm->floatStack, accum);
    if (forthcode && (vm->state == FICL_VM_STATE_COMPILE))
        ficlPrimitiveFLiteralImmediate(vm);

    return(1);
}
/**************************************************************************
                     f i c l P a r s e F l o a t N u m b e r
** vm -- Virtual Machine pointer.
** s -- String to parse.
** Returns 1 if successful, 0 if not.
**************************************************************************/
int ficlVmParseFloatNumber( ficlVm *vm, ficlString s)
{
    return parseFloatNumber(vm, s, 1);
}

static void ficlPrimitiveToFloat(ficlVm *vm)
{
ficlString s;
    FICL_STACK_CHECK(vm->dataStack, 2, 0);
    FICL_STRING_SET_LENGTH(s, ficlStackPopInteger(vm->dataStack));
    FICL_STRING_SET_POINTER(s, ficlStackPopPointer(vm->dataStack));
    if (!parseFloatNumber(vm, s, 0)) {
	ficlVmThrowError(vm, "Floating point number invalid");
    }
    return;
}

#if 0
static void ficlPrimitiveDoubleToFloat(ficlVm *vm)
{
    FICL_STACK_CHECK(vm->dataStack, 2, 0);
    FICL_FLOAT_STACK_CHECK(vm->floatStack, 0, 1);
    ficlStackPushFloat(vm->floatStack,
	(float)ficlStackPop2Integer(vm->dataStack));
    return;
}

static void ficlPrimitiveFloatToDouble(ficlVm *vm)
{
    FICL_STACK_CHECK(vm->dataStack, 0, 2);
    FICL_FLOAT_STACK_CHECK(vm->floatStack, 1, 0);
    ficlStackPush2Integer(vm->dataStack,
	(ficl2Integer)ficlStackPopFloat(vm->floatStack));
    return;
}
#endif

#if FICL_WANT_LOCALS

static void ficlPrimitiveFLocalParen(ficlVm *vm)
{
   ficlLocalParen(vm, 0, 1);
}

#endif /* FICL_WANT_LOCALS */

/**************************************************************************
** Add float words to a system's dictionary.
** system -- Pointer to the Ficl sytem to add float words to.
**************************************************************************/
void ficlSystemCompileFloat(ficlSystem *system)
{
    ficlDictionary *dictionary = ficlSystemGetDictionary(system);
    ficlDictionary *environment = ficlSystemGetEnvironment(system);

    FICL_SYSTEM_ASSERT(system, dictionary);
    FICL_SYSTEM_ASSERT(system, environment);

    ficlDictionarySetPrimitive(dictionary, "fconstant", ficlPrimitiveFConstant,      FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fvalue", ficlPrimitiveFConstant,      FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fdepth",    ficlPrimitiveFDepth,         FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fliteral",  ficlPrimitiveFLiteralImmediate,     FICL_WORD_IMMEDIATE);
    ficlDictionarySetPrimitive(dictionary, "f.",        ficlPrimitiveFDot,           FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "f.s",       ficlVmDisplayFloatStack,  FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fe.",       ficlPrimitiveEDot,           FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, ">float",    ficlPrimitiveToFloat,         FICL_WORD_DEFAULT);
    // ficlDictionarySetPrimitive(dictionary, "f>d",       ficlPrimitiveFloatToDouble,         FICL_WORD_DEFAULT);
    // ficlDictionarySetPrimitive(dictionary, "d>f",       ficlPrimitiveDoubleToFloat,         FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "faligned",  ficlPrimitiveFAligned,         FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "falign",    ficlPrimitiveFAlign,         FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fvariable", ficlPrimitiveFVariable,         FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "f~", 	ficlPrimitiveFTilde,         FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "floor", ficlPrimitiveFloor, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fmax", ficlPrimitiveFMax, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fmin", ficlPrimitiveFMin, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fround", ficlPrimitiveFRound, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fabs", ficlPrimitiveFAbs, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "facos", ficlPrimitiveFACos, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "facosh", ficlPrimitiveFACosH, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "falog", ficlPrimitiveFALog, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fasin", ficlPrimitiveFASin, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fasinh", ficlPrimitiveFASinH, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fatan", ficlPrimitiveFATan, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fatan2", ficlPrimitiveFATan2, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fatanh", ficlPrimitiveFATanH, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fcos", ficlPrimitiveFCos, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fcosh", ficlPrimitiveFCosH, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fexp", ficlPrimitiveFExp, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fexpm1", ficlPrimitiveFExpM1, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fln", ficlPrimitiveFLn, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "flnp1", ficlPrimitiveFLnP1, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "flog", ficlPrimitiveFLog, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fsin", ficlPrimitiveFSin, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fsinh", ficlPrimitiveFSinH, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fsqrt", ficlPrimitiveFSqrt, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "ftan", ficlPrimitiveFTan, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "ftanh", ficlPrimitiveFTanH, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "ftrunc", ficlPrimitiveFTrunc, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "f**", ficlPrimitiveFStarStar, FICL_WORD_DEFAULT);
    ficlDictionarySetPrimitive(dictionary, "fsincos", ficlPrimitiveFSinCos, FICL_WORD_DEFAULT);

#if FICL_WANT_LOCALS
    ficlDictionarySetPrimitive(dictionary, "(flocal)",   ficlPrimitiveFLocalParen,     FICL_WORD_COMPILE_ONLY);
#endif /* FICL_WANT_LOCALS */

 /* 
    Missing words:

    d>f
    f>d 
    falign 
    faligned 
    float+
    floats
    floor
    fmax
    fmin
*/

    ficlDictionarySetConstant(environment, "floating",       FICL_TRUE);
    ficlDictionarySetConstant(environment, "floating-ext",   FICL_FALSE);
    ficlDictionarySetConstant(environment, "floating-stack", system->stackSize);
    return;
}
